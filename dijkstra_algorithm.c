int dijkstra() {
	for (i = 1; i <= N; i++) {
		dist[i] = INF;
	}

	dist[1] = 0;

	for (i = 1; i <= N; i++) {
		min = INF;
		for (j = 1; j <= N; j++) {
			if ((min > dist[j]) && flag[j] == 0) {   
				min = dist[j];
				position = j; //현재 정점 저장
				printf("min: %d ", min);
			}
		}
		puts("");
		flag[position] = 1;//최소 정점 확정

		for (j = 1; j <= N; j++) {
			if (dist[j] > data[position][j] + dist[position] && data[position][j] != INF) {//현재 position에서 갈수 있는 노드 중 dist 갱신
				dist[j] = data[position][j] + dist[position];
				printf("dist[%d] %d p:%d \n", j,dist[j],position);
				path[j] = position;
			}
		}

	}
	for (int i = 1; i <= N; i++) {
		printf("1~%d:  %d  ", i, dist[i]); //최소거리 출력

		position = i;
		printf("%d", i);
		do{									//경로 출력
			if(path[position])
				printf("<-%d", path[position]);
			position = path[position];
		} while (path[position]);
		puts("");
	}
}
