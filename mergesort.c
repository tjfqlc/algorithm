#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define SIZE 30000

int sum;

void random(int data[]);
void mergeSort(int data[], int p, int r);
void merge(int data[], int p, int q, int r);

int main() {
	clock_t start, end;
	float result;
	start = clock(); // 측정 시작

	int data[SIZE] = {}, i;
	random(data);
	printf("정렬 전 \n");
	

	mergeSort(data, 0, SIZE-1);
	

	end = clock(); // 측정끝
	result = float(end - start)/CLOCKS_PER_SEC;
	printf("\n\n실행시간: %.3fs\n", result);

	return 0;
}

void mergeSort(int data[], int p, int r) {
	int q;
	if (p < r) {	//배열 원소가 2개 이상일떄
		q = (p + r) / 2;
		mergeSort(data, p, q);
		mergeSort(data, q + 1, r);
		merge(data, p, q, r);
	}
}

void merge(int data[], int p, int q, int r) { 
	int i = p, j = q + 1, k = p;
	int tmp[SIZE]; //원소들을 넣을 배열
	while (i <= q && j <= r){  //양쪽에 원소가 남아있으면
		if (data[i] <= data[j]) tmp[k++] = data[i++];
		else tmp[k++] = data[j++];
	}
	while (i <= q) tmp[k++] = data[i++]; 
	while (j <= r) tmp[k++] = data[j++];

	for (int a = p; a <= r; a++) data[a] = tmp[a];
}
void random(int data[]) {
	srand(time(NULL));
	for (int i = 0; i < SIZE; i++)
	{
		data[i] = rand() % 50001;
		for (int j = 0; j < i; j++) {
			if (data[i] == data[j])
				i--;
		}
	}
}


